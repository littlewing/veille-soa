package example;

import javax.servlet.ServletInputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletRequestWrapper;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;

/**
 * Classe permettant de mettre en cache le payload et d'analyser le flux d entree
 * User: touret-a
 * Date: 14/09/12
 * Time: 15:59
 */
public class BufferedRequestWrapper extends HttpServletRequestWrapper {
    ByteArrayInputStream byteArrayInputStream;

    ByteArrayOutputStream byteArrayOutputStream;

    BufferedServletInputStream bufferedServletInputStream;

    byte[] buffer;

    /**
     * Contructeur par defaut
     *
     * @param req la requete en entree
     * @throws IOException
     */
    public BufferedRequestWrapper(HttpServletRequest req) throws IOException {
        super(req);
        if (req.getContentLength() > 0) {
            InputStream is = req.getInputStream();
            byteArrayOutputStream = new ByteArrayOutputStream();

            byte buf[] = new byte[req.getContentLength()];
            int c;
            while ((c = is.read(buf)) > 0) {
                byteArrayOutputStream.write(buf, 0, c);
            }
            buffer = byteArrayOutputStream.toByteArray();
        }
    }

    /**
     * Retourne l inputstream place en cache
     *
     * @return le flux d entree de la requete
     * @see #BufferedRequestWrapper(javax.servlet.http.HttpServletRequest)
     */
    public ServletInputStream getInputStream() {
        try {
            byteArrayInputStream = new ByteArrayInputStream(buffer);
            bufferedServletInputStream = new BufferedServletInputStream(byteArrayInputStream);
        } catch (Exception ex) {
            ex.printStackTrace();
        }

        return bufferedServletInputStream;
    }

    /**
     * Retourne le payload serialise
     *
     * @return le flux d entree
     */
    public byte[] getBuffer() {
        return buffer;
    }
}
