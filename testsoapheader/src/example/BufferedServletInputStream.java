package example;

import javax.servlet.ServletInputStream;
import java.io.ByteArrayInputStream;

/**
 * User: touret-a
 * Date: 14/09/12
 * Time: 15:58
 */
public class BufferedServletInputStream extends ServletInputStream {


    ByteArrayInputStream arrayInputStream;

    public BufferedServletInputStream(ByteArrayInputStream arrayInputStream) {
        this.arrayInputStream = arrayInputStream;
    }

    public int available() {
        return arrayInputStream.available();
    }

    public int read() {
        return arrayInputStream.read();
    }

    public int read(byte[] buf, int off, int len) {
        return arrayInputStream.read(buf, off, len);
    }


}
