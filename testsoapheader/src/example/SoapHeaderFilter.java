package example;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import java.io.IOException;

/**
 * Filtre permettant de récupérer le message d'un message SOAP
 * User: touret-a
 * Date: 14/09/12
 * Time: 11:56
 */
public class SoapHeaderFilter implements Filter {

    private FilterConfig filterConfig;

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {
        this.filterConfig = filterConfig;
    }

    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {

        HttpServletRequest httpServletRequest = (HttpServletRequest) servletRequest;
        //Transformation de la reqsuete
        BufferedRequestWrapper bufferedRequestWrapper = new BufferedRequestWrapper(httpServletRequest);

        System.out.println("---> " + bufferedRequestWrapper.getBuffer());
        // retour a la normale
        filterChain.doFilter(bufferedRequestWrapper, servletResponse);
    }

    @Override
    public void destroy() {
    }
}
