package example;

import javax.xml.namespace.QName;
import javax.xml.soap.SOAPEnvelope;
import javax.xml.soap.SOAPException;
import javax.xml.soap.SOAPHeader;
import javax.xml.soap.SOAPHeaderElement;
import javax.xml.ws.handler.MessageContext;
import javax.xml.ws.handler.soap.SOAPHandler;
import javax.xml.ws.handler.soap.SOAPMessageContext;
import java.util.HashSet;
import java.util.Set;

/**
 * Handler permettant de manipuler les en tetes du message entrant
 * User: touret-a
 * Date: 14/09/12
 * Time: 16:28
 */
public class UsernameHandler implements SOAPHandler<SOAPMessageContext> {


    public static final QName QNAME = new QName("http://example/", "username");

    @Override
    public Set<QName> getHeaders() {
        Set<QName> qNames = new HashSet<QName>(1);
        qNames.add(QNAME);
        return qNames;

    }

    @Override
    public boolean handleMessage(SOAPMessageContext context) {
        Boolean isInbound =
                !(Boolean) context.get(MessageContext.MESSAGE_OUTBOUND_PROPERTY);
        /*
        verifie si le message est entrant
        * */
        if (isInbound) {
            try {
                System.err.println("message entrant en cours d analyse...");
                SOAPEnvelope soapEnvelope = context.getMessage().getSOAPPart().getEnvelope();
                SOAPHeader soapHeader = soapEnvelope.getHeader();
                SOAPHeaderElement soapHeaderElement = soapHeader.addHeaderElement(QNAME);
                soapHeaderElement.setTextContent("SCLPRD");
            } catch (SOAPException e) {
            }
        }
        return true;

    }

    @Override
    public boolean handleFault(SOAPMessageContext context) {
       return true;
    }

    @Override
    public void close(MessageContext context) {

    }
}
